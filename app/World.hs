{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module World where

import Apecs
import Apecs.Gloss

type Loc = (Float, Float)

distance :: Loc -> Loc -> Float
distance (x1 , y1) (x2 , y2) = sqrt (x'*x' + y'*y')
    where
      x' = x1 - x2
      y' = y1 - y2

bubbleRadius :: Float
bubbleRadius = 20.0

data Bubble = Bubble
  { _bubbleLoc :: Loc
  }

instance Component Bubble where type Storage Bubble = Map Bubble

totalFailedTime :: Float
totalFailedTime = 1.0

data FailedBubble = FailedBubble
  { _failedLoc   :: Loc
  , _failedTime  :: Float
  }
instance Component FailedBubble where type Storage FailedBubble = Map FailedBubble

data Cursor = Cursor
  { _cursorLoc :: Loc
  }

instance Component Cursor where type Storage Cursor = Global Cursor

instance Semigroup Cursor where
  c1 <> _ = c1

instance Monoid Cursor where
  mempty = Cursor { _cursorLoc = (0, 0) }

makeWorld "World"
  [ ''Bubble
  , ''FailedBubble
  , ''Cursor
  , ''Camera -- for apecs-gloss
  ]

