{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}

module Main where

import Control.Monad (when)
import Data.Foldable (for_)
import Data.Functor (void)
import Data.Monoid (All (..))

import Apecs
import Apecs.Gloss
import Apecs.Core

import World

cfoldMap
  :: forall w m c a
   . Members w m c
  => Get w m c
  => Monoid a
  => (c -> a)
  -> SystemT w m a
cfoldMap f = cfold (\acc c -> mappend acc (f c)) mempty

handleEvent :: Event -> System World ()
handleEvent = \case
  EventMotion loc -> set global Cursor { _cursorLoc = loc }
  EventKey (MouseButton LeftButton) Down _ _ -> do
    Cursor{..} <- get global
    -- Check for collisions with any bubble (circles are easy to calculate collision for)
    All hasRoom <- cfoldMap $ \Bubble{..} -> All $ distance _bubbleLoc _cursorLoc > (bubbleRadius * 2)
    if hasRoom
      then void $ newEntity Bubble { _bubbleLoc = _cursorLoc }
      else void $ newEntity FailedBubble { _failedLoc = _cursorLoc, _failedTime = totalFailedTime }
  EventKey (MouseButton RightButton) Down _ _ -> do
    Cursor{..} <- get global
    -- You can right click to remove bubbles for good measure
    cmap $ \Bubble{..} -> if distance _bubbleLoc _cursorLoc < bubbleRadius then Nothing else Just Bubble{..}
  _ -> pure ()

-- The only thing we step is our FailedBubble animation
step :: Float -> System World ()
step dt = cmap $ \FailedBubble{..} -> do
  let newTime = _failedTime - dt
  if newTime <= 0 then Nothing else Just FailedBubble{ _failedTime = newTime, .. }

draw :: System World Picture
draw = mconcat <$> sequence
       [ cfoldMap $ \Bubble{..} -> uncurry Translate _bubbleLoc $ color aquamarine $ circleSolid bubbleRadius
       , cfoldMap $ \FailedBubble{..} ->
           uncurry Translate _failedLoc $ color (withAlpha (_failedTime / totalFailedTime) rose) $ circleSolid bubbleRadius
       , do
           Cursor{..} <- get global
           pure $ uncurry Translate _cursorLoc $ color white $ circle bubbleRadius
       ]

main :: IO ()
main = do
  w <- initWorld
  runWith w $ do
    play FullScreen black 60 draw handleEvent step
